const express = require("express");
const controllers = require("../app/controllers");
const apiRouter = express.Router();


// configure and initialization swagger
const swaggerUi = require('swagger-ui-express');
const swaggerDocument = require('./swagger.json');

apiRouter.use('/api-docs', swaggerUi.serve);
apiRouter.get('/api-docs', swaggerUi.setup(swaggerDocument));

apiRouter.post(
    "/api/v1/users/register",
    controllers.api.v1.userControllers.register
);
apiRouter.post(
    "/api/v1/users/login",
    controllers.api.v1.userControllers.login
);
apiRouter.get(
    "/api/v1/users",
    controllers.api.v1.userControllers.authorize,
    controllers.api.v1.userControllers.superAdmin,
    controllers.api.v1.userControllers.list
);
apiRouter.put(
    "/api/v1/users/:id",
    controllers.api.v1.userControllers.authorize,
    controllers.api.v1.userControllers.superAdmin,
    controllers.api.v1.userControllers.update
);


// whoami
apiRouter.get(
    "/api/v1/users/whoami",
    controllers.api.v1.userControllers.authorize,
    controllers.api.v1.userControllers.whoAmI
);

apiRouter.post(
    "/api/v1/cars",
    controllers.api.v1.userControllers.authorize,
    controllers.api.v1.userControllers.admin_superAdmin,
    controllers.api.v1.carsControllers.create
);

// Update Cars
apiRouter.put(
    "/api/v1/cars/:id",
    controllers.api.v1.userControllers.authorize,
    controllers.api.v1.userControllers.admin_superAdmin,
    controllers.api.v1.carsControllers.update
);

// Show Cars By ID
apiRouter.get(
    "/api/v1/listcars/:id",
    controllers.api.v1.userControllers.authorize,
    controllers.api.v1.userControllers.admin_superAdmin,
    controllers.api.v1.carsControllers.showCars
);

// Delete Cars
apiRouter.delete(
    "/api/v1/cars/:id",
    controllers.api.v1.userControllers.authorize,
    controllers.api.v1.userControllers.admin_superAdmin,
    controllers.api.v1.carsControllers.deletedCar
);

apiRouter.get(
    "/api/v1/deleted-cars",
    controllers.api.v1.userControllers.authorize,
    controllers.api.v1.userControllers.admin_superAdmin,
    controllers.api.v1.carsControllers.listCarDeleted
);

// Show All Cars List
apiRouter.get("/api/v1/listcars", controllers.api.v1.userControllers.authorize, controllers.api.v1.carsControllers.list);

/**
 * TODO: Implement your own API
 *       implementations
 */

apiRouter.get("/api/v1/errors", () => {
    throw new Error("The Industrial Revolution and its consequences have been a disaster for the human race.");
});

apiRouter.use(controllers.api.main.onLost);
apiRouter.use(controllers.api.main.onError);

module.exports = apiRouter;