'use strict';

module.exports = {
    async up(queryInterface, Sequelize) {
        /**
         * Add seed commands here.
         *
         * Example:
         * await queryInterface.bulkInsert('People', [{
         *   name: 'John Doe',
         *   isBetaMember: false
         * }], {});
         */
        await queryInterface.bulkInsert(
            "Users", [{
                    username: "superadmin",
                    email: "superadmin@gmail.com",
                    password: "$2a$10$rv88oMxG3GK7JPiQ.4tHfOLSrMnwIPHtl/apF61kpCPHOMvacQoX6",
                    role: "superadmin",
                    createdAt: new Date(),
                    updatedAt: new Date(),
                },
                {
                    username: "admin",
                    email: "admin@gmail.com",
                    password: "$2a$10$rv88oMxG3GK7JPiQ.4tHfOLSrMnwIPHtl/apF61kpCPHOMvacQoX6",
                    role: "admin",
                    createdAt: new Date(),
                    updatedAt: new Date(),
                },
            ],
        );

    },

    async down(queryInterface, Sequelize) {
        /**
         * Add commands to revert seed here.
         *
         * Example:
         * await queryInterface.bulkDelete('People', null, {});
         */
    }
};