'use strict';
module.exports = {
    async up(queryInterface, Sequelize) {
        await queryInterface.createTable('Cars', {
            id: {
                allowNull: false,
                autoIncrement: true,
                primaryKey: true,
                type: Sequelize.INTEGER
            },
            plate: {
                type: Sequelize.STRING,
                allowNull: false
            },
            model: {
                type: Sequelize.STRING,
                allowNull: false
            },
            type: {
                type: Sequelize.STRING,
                allowNull: false
            },
            capacity: {
                type: Sequelize.STRING,
                allowNull: false
            },
            rent: {
                type: Sequelize.INTEGER
            },

            isDeleted: {
                type: Sequelize.BOOLEAN,
            },
            createdBy: {
                type: Sequelize.STRING,
            },
            updatedBy: {
                type: Sequelize.STRING,
            },
            deletedBy: {
                type: Sequelize.STRING,
            },
            createdAt: {
                allowNull: false,
                type: Sequelize.DATE
            },
            updatedAt: {
                allowNull: false,
                type: Sequelize.DATE
            }
        });
    },
    async down(queryInterface, Sequelize) {
        await queryInterface.dropTable('Cars');
    }
};