"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
    class Users extends Model {
        /**
         * Helper method for defining associations.
         * This method is not a part of Sequelize lifecycle.
         * The `models/index` file will call this method automatically.
         */
        static associate(models) {

        }
    }
    Users.init({
        username: {
            type: DataTypes.STRING,
            allowNull: false,
        },
        email: {
            type: DataTypes.STRING,
            unique: {
                args: true,
                msg: 'Email already exist'
            },
            validate: {
                isLowercase: true,
                notEmpty: {
                    msg: 'Please input your email'
                },
                isEmail: {
                    msg: 'Email is invalid'
                }
            }
        },
        password: {
            type: DataTypes.STRING,
            allowNull: false,
        },
        role: {
            type: DataTypes.STRING,
            defaultValue: "member",
        },
    }, {
        sequelize,
        modelName: "Users",
    });
    return Users;
};