'use strict';
const {
    Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
    class Cars extends Model {
        /**
         * Helper method for defining associations.
         * This method is not a part of Sequelize lifecycle.
         * The `models/index` file will call this method automatically.
         */
        static associate(models) {
            // define association here
        }
    }
    Cars.init({
        plate: DataTypes.STRING,
        model: DataTypes.STRING,
        type: DataTypes.STRING,
        capacity: DataTypes.INTEGER,
        rent: DataTypes.INTEGER,
        isDeleted: {
            type: DataTypes.BOOLEAN,
            defaultValue: false,
        },
        createdBy: {
            type: DataTypes.STRING,
            defaultValue: null,
        },
        updatedBy: {
            type: DataTypes.STRING,
            defaultValue: null,
        },
        deletedBy: {
            type: DataTypes.STRING,
            defaultValue: null,
        }

    }, {
        sequelize,
        modelName: 'Cars',
    });
    return Cars;
};