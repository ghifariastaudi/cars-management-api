const userRepo = require("../repositories/userRepo");

module.exports = {
    create(requestBody) {
        return userRepo.create(requestBody);
    },

    update(id, requestBody) {
        return userRepo.update(id, requestBody);
    },
    login(requestBody) {
        return userRepo.login(requestBody);
    },

    delete(id) {
        return userRepo.delete(id);
    },

    async list() {
        try {
            const users = await userRepo.findAll();
            const userCount = await userRepo.getTotalUser();

            return {
                data: users,
                count: userCount,
            };
        } catch (err) {
            throw err;
        }
    },

    get(id) {
        return userRepo.find(id);
    },
};