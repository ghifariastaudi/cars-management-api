const carsRepo = require("../repositories/carsRepo");

module.exports = {
    create(requestBody) {
        return carsRepo.create(requestBody);
    },

    update(id, requestBody) {
        return carsRepo.update(id, requestBody);
    },

    isCarDeleted(id, requestBody) {
        return carsRepo.isCarDeleted(id, requestBody);
    },

    delete(id) {
        return carsRepo.delete(id);
    },

    async list(args) {
        try {
            const cars = await carsRepo.findAll(args);
            const carCount = await carsRepo.getTotalCar(args);

            return {
                data: cars,
                count: carCount,
            };
        } catch (err) {
            throw err;
        }
    },

    get(id) {
        return carsRepo.find(id);
    },
};