const userService = require("../../../services/userServices");

const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
const { User } = require("../../../models");
const SALT = 10;

function encryptPassword(password) {
    return new Promise((resolve, reject) => {
        bcrypt.hash(password, SALT, (err, encryptedPassword) => {
            if (!!err) {
                reject(err);
                return;
            }

            resolve(encryptedPassword);
        });
    });
}

function checkPassword(encryptedPassword, password) {
    return new Promise((resolve, reject) => {
        bcrypt.compare(password, encryptedPassword, (err, isPasswordCorrect) => {
            if (!!err) {
                reject(err);
                return;
            }

            resolve(isPasswordCorrect);
        });
    });
}

function createToken(payload) {
    return jwt.sign(payload, process.env.JWT_SIGNATURE_KEY || "Rahasia", {
        expiresIn: "12h", // it will be expired after 120 hours
    });
}
module.exports = {
    list(req, res) {

        userService
            .list()
            .then(({ data, count }) => {
                res.status(200).json({
                    status: "OK",
                    data: { users: data },
                    meta: { total: count },
                });
            })
            .catch((err) => {
                res.status(400).json({
                    status: "FAIL",
                    message: err.message,
                });
            });
    },
    async register(req, res) {
        const { email, username } = req.body;
        const password = await encryptPassword(req.body.password);
        userService.create({
                email: email.toLowerCase(),
                username,
                password: password
            })
            .then((user) => {
                res.status(201).json({
                    status: "Success",
                    message: "User Successfully Registered!",
                    data: user
                });
            }).catch((err) => {
                res.status(400).json({
                    status: "FAIL",
                    message: err.message,
                });
            });

    },
    async login(req, res) {
        const email = req.body.email.toLowerCase(); // Biar case insensitive
        const password = req.body.password;

        const user = await userService.login(email)

        if (!user) {
            res.status(404).json({ message: "Email tidak ditemukan" });
            return;
        }
        const isPasswordCorrect = await checkPassword(
            user.password,
            password
        );

        if (!isPasswordCorrect) {
            res.status(401).json({ message: "Password salah!" });
            return;
        }

        const token = createToken({
            id: user.id,
            email: user.email,
            role: user.role,
            createdAt: user.createdAt,
            updatedAt: user.updatedAt,
        });

        res.status(200).json({
            statusLogin: "Berhasil",
            id: user.id,
            email: user.email,
            role: user.role,
            token, // Kita bakal ngomongin ini lagi nanti.
            createdAt: user.createdAt,
            updatedAt: user.updatedAt,
        });
    },
    async authorize(req, res, next) {
        try {
            const bearerToken = req.headers.authorization;
            const token = bearerToken.split("Bearer ")[1];
            const tokenPayload = jwt.verify(
                token,
                process.env.JWT_SIGNATURE_KEY || "Rahasia"
            );

            req.user = await userService.get(tokenPayload.id);
            next();
        } catch (err) {
            console.error(err);
            res.status(401).json({
                message: "Unauthorized",
            });
        }
    },

    async update(req, res) {
        try {
            const users = await userService.update(req.params.id, req.body);
            res.status(200).json({
                status: "Successfully updated",
                data: users
            })
        } catch (err) {
            res.status(400).json({
                status: "Failed",
                errors: [err.message]
            })
        }

    },
    delete(req, res) {
        userService.delete({
                where: {
                    id: req.params.id
                }
            })
            .then(() => {
                res.status(200).json({
                    status: "OK",
                    message: `User with id ${req.params.id} has been deleted.`
                })
            }).catch((err) => {
                res.status(422).json({
                    status: "FAIL",
                    message: err.message,
                });
            });
    },
    show(req, res) {
        userService
            .get(req.params.id)
            .then((user) => {
                res.status(200).json({
                    status: "OK",
                    data: user,
                });
            })
            .catch((err) => {
                res.status(422).json({
                    status: "FAIL",
                    message: err.message,
                });
            });
    },

    async whoAmI(req, res) {
        res.status(200).json(req.user);
    },
    async admin_superAdmin(req, res, next) {
        if (!(req.user.role === "superadmin" || req.user.role === "admin")) {
            res.json({
                message: "You are not superadmin or admin, therefore you're not allowed to continue"
            });
            return;
        }
        next();
    },

    async superAdmin(req, res, next) {
        if (!(req.user.role === "superadmin")) {
            res.json({
                message: "You're not superadmin",
            });
            return;
        }
        next();
    },


}