/**
 * @file contains request handler of post resource
 * @author Fikri Rahmat Nurhidayat
 */
const carService = require("../../../services/carsService");

module.exports = {
    create(req, res) {
        req.body.createdBy = req.user.username;
        carService
            .create(req.body)
            .then((car) => {
                res.status(201).json({
                    status: "OK",
                    message: `Successfully created new car by ${req.user.username}`,
                    data: car,
                });
            })
            .catch((err) => {
                res.status(422).json({
                    status: "FAIL",
                    message: err.message,
                });
            });
    },
    list(req, res) {
        carService
            .list({
                where: { isDeleted: false },
            })
            .then(({ data, count }) => {
                res.status(200).json({
                    status: "OK",
                    data: { cars: data },
                    meta: { total: count },
                });
            })
            .catch((err) => {
                res.status(400).json({
                    status: "FAIL",
                    message: err.message,
                });
            });
    },
    listCarDeleted(req, res) {
        carService
            .list({
                where: { isDeleted: true },
            }).then((allCars) => {
                res.status(200).json({
                    status: "success",
                    data: {
                        allCars
                    }
                })
            }).catch((err) => {
                res.status(400).json({
                    status: "FAIL",
                    message: err.message
                })
            })
    },
    update(req, res) {
        req.body.updatedBy = req.user.username;
        carService
            .update(req.params.id, req.body)
            .then(() => {
                res.status(200).json({
                    status: "OK",
                    message: `Car Success Created by ${req.user.username}`,
                });
            })
            .catch((err) => {
                res.status(422).json({
                    status: "FAIL",
                    message: err.message,
                });
            });
    },

    showCars(req, res) {
        carService
            .get(req.params.id)
            .then((post) => {
                res.status(200).json({
                    status: "OK",
                    data: post,
                });
            })
            .catch((err) => {
                res.status(422).json({
                    status: "FAIL",
                    message: err.message,
                });
            });
    },

    deletedCar(req, res) {
        carService
            .isCarDeleted(req.params.id, { isDeleted: true, deletedBy: req.user.username })
            .then((car) => {
                res.status(200).json({
                    status: "OK",
                    deletedBy: req.user.username,
                });
            })
            .catch((err) => {
                res.status(422).json({
                    status: "FAIL",
                    message: err.message,
                });
            });
    },
};