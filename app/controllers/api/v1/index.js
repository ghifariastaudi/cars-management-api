/**
 * @file contains entry point of controllers api v1 module
 * @author Fikri Rahmat Nurhidayat
 */

const userControllers = require("./userControllers");
const carsControllers = require("./carsControllers");


module.exports = {
    userControllers,
    carsControllers
};