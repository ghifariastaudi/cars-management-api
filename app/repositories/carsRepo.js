const { Cars } = require("../models");

module.exports = {
    create(createArgs) {
        return Cars.create(createArgs);
    },

    update(id, updateArgs) {
        return Cars.update(updateArgs, {
            where: {
                id,
            },
        });
    },

    isCarDeleted(id, updateArgs) {
        return Cars.update(updateArgs, {
            where: {
                id,
            },
        });
    },

    delete(id) {
        return Cars.destroy({
            where: {
                id,
            },
        });
    },

    find(id) {
        return Cars.findOne({
            where: {
                id,
            },
        });
    },

    findAll(args) {
        return Cars.findAll(args);
    },

    getTotalCar(args) {
        return Cars.count(args);
    },

};