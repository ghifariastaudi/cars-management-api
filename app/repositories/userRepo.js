const { Users } = require("../models");

module.exports = {
    create(createArgs) {
        return Users.create(createArgs);
    },

    login(email) {
        return Users.findOne({
            where: { email },
        })
    },

    update(id, updateArgs) {
        return Users.update(updateArgs, {
            where: {
                id,
            },
        });
    },

    delete(id) {
        return Users.destroy(id);
    },

    find(id) {
        return Users.findByPk(id);
    },

    findAll() {
        return Users.findAll();
    },

    getTotalUser() {
        return Users.count();
    },
};